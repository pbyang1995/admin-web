UPDATE  `a63c4c80f0f54`.`tb_user` SET  `u_password` =  'VIlO0st6mlpQF70XwXyiGA==',
`u_credentials_salt` =  '6ca87d4b7820b5fd90bd821b8af1ecbc' WHERE  `tb_user`.`u_id` =4;

文档参考地址：http://git.oschina.net/wjggwm/webside/wikis

项目部署说明：
1、mysql数据库，只需要先手动执行如下sql脚本即可，其他系统会自动处理

CREATE TABLE `webside`.`tb_schema_version` (
    `installed_rank` INT NOT NULL,
    `version` VARCHAR(50),
    `description` VARCHAR(200) NOT NULL,
    `type` VARCHAR(20) NOT NULL,
    `script` VARCHAR(1000) NOT NULL,
    `checksum` INT,
    `installed_by` VARCHAR(100) NOT NULL,
    `installed_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `execution_time` INT NOT NULL,
    `success` BOOL NOT NULL,
    -- Add the primary key as part of the CREATE TABLE statement in case `innodb_force_primary_key` is enabled
    CONSTRAINT `TB_SCHEMA_VERSION_pk`PRIMARY KEY (`installed_rank`)
) ENGINE=InnoDB;

系统的超级管理员账户和密码为：admin@webside.com/admin123
webside是基于RBAC的完全响应式权限管理系统，包括用户管理、角色管理，权限管理等功能，适合javaweb开发者入门学习，也可直接用于项目，省去重复开发权限管理模块，提高开发效率，项目使用主流技术如下：

服务端：
Spring4.1.6.RELEASE+SpringMVC4.1.6.RELEASE+Mybatis3.3.0+Shiro1.2.4+druid1.0.14+ehcache2.6.11 等

前端：
JQuery+Bootstrap3.3.5+ACE1.3.4（基于bootstrap的响应式后台管理模板）+layer+DTGrid+JQuery validation等

特点：
1、简单，项目代码均添加注释，阅读方便
2、精简，采用经典的MVC模式，对数据访问层和业务逻辑层进行了抽象，大大提高开发效率
3、快速，可以直接用于项目，只关注项目的核心功能模块，而无需重复开发权限管理模块
4、高逼格，项目使用maven进行构建
5、多维监控，添加druid监控和sirona监控，多重监控系统性能及各项指标
6、跨浏览器支持，IE、360、google、firefox 等主流浏览器

功能：
1、用户管理
2、角色管理
3、权限资源管理
4、druid监控&sirona监控
5、登录日志&操作日志监控
6、多级菜单支持，最多支持四级
7、基于quartz动态定时任务


开发计划(不分先后)：
1、quartz计划任务(已实现)
2、restful api 支持
4、Shiro Ajax请求权限不满足，拦截后解决方案(已实现)
5、shiro + redis(NoSql) 集成(已实现)
6、管理员权限的自动添加(当有一个权限创建，自动添加到管理员角色下，保证管理员是最大权限)(已实现)
7、RPC服务支持
	7.1、Motan(是一套高性能、易于使用的分布式远程服务调用(RPC)框架)
	7.2、Dubbox(支持REST风格远程调用（HTTP + JSON/XML)、支持基于Kryo和FST的Java高效序列化实现、支持基于嵌入式Tomcat的HTTP remoting体系)等
8、mysql集群
9、keepalive+Nginx 高可用web负载均衡
10、windows、linux下分布式集群部署
11、集成ip2region模块-ip到地区的映射库，实现基于ip的定位(已实现)
12、flume-ng+Kafka+Storm+HDFS 实时系统搭建(http://www.aboutyun.com/thread-6855-1-1.html)


webside系统bug：
2016-07-07
1、导出全部数据时日志报空指针异常 (已修复)
2、添加角色资源时赋权限给超级管理员异常(已修复)
3、添加用户事务管理优化(已修复)

dtgrid bug修复记录：
1、dlshouwen.grid.v1.2.1  分页条页码大小不一，分页说明和页面导航垂直不对齐的修改方法
	1、页面大小不一：bootstrap.min.css 中搜索 .pagination>li>a,.pagination>li>span 将 float:left 取消掉
	2、垂直不对齐：dlshouwen.grid.min.css 中搜索.dlshouwen-grid-toolbar-container .dlshouwen-grid-pager .dlshouwen-grid-pager-status 修改为float:right;margin:-3px 0px 0px 0px;height:28px;line-height:28px;
2、修复火狐浏览器下分页页码水平不对齐的bug
	1、在dlshouwen.grid.min.css 中搜索.dlshouwen-grid-toolbar-container .dlshouwen-grid-pager .dlshouwen-grid-pager-button 修改为float:right;margin:2px 0px 11px 10px;white-space: nowrap;
3、工具栏调整
	1、在dlshouwen.grid.min.css 中搜索.dlshouwen-grid-toolbar-container .dlshouwen-grid-tools 修改为 float:left;margin:0px 0px 12px 0px
4、导出全部数据有后台报空指针异常
	1、修改GridUtils.java 中 else if("number".equalsIgnoreCase(column.getType())&&!"".equals(column.getFormat())) 为  else if("number".equalsIgnoreCase(column.getType())&&!"".equals(column.getFormat())&& null != column.getFormat()) 
5、修改loadAll=true时是否传递参数到后台
	添加表格属性：postParams //是否传递参数,只在loadAll=true时有效
6、修改loadAll=true时刷新时是否重新从服务器获取数据
	添加表格属性：isreload //刷新时是否重新从服务器获取数据,只在loadAll=true时有效
	
7、修复firefox下表格工具栏样式错乱bug
	1、在dlshouwen.grid.min.css 中搜索.dlshouwen-grid-toolbar-container .dlshouwen-grid-tools 修改为 float:left;margin:0px 0px 12px 0px;white-space:nowrap;
	2、在dlshouwen.grid.min.css 中搜索.change-page-size 修改为width:68px;display:inline;padding:2px 4px;line-height:0;height:29px;

附加知识点：
quartz cron 说明：
Cron表达式时间字段：
格式: [秒] [分] [小时] [日] [月] [周] [年]
序号	说明	是否必填	允许填写的值			允许的通配符
1	秒	是		0-59 				, - * /
2	分	是		0-59				, - * /
3	小时	是		0-23				, - * /
4	日	是		1月31日				, - * ? / L W
5	月	是		1-12 or JAN-DEC		, - * /
6	周	是		1-7 or SUN-SAT		, - * ? / L #
7	年	否		empty 或 1970-2099	, - * /

 通配符说明:
* ：表示所有值. 例如:在分的字段上设置 "*",表示每一分钟都会触发。
? ：表示不指定值。使用的场景为不需要关心当前设置这个字段的值。例如:要在每月的10号触发一个操作，但不关心是周几，所以需要周位置的那个字段设置为"?" 具体设置为 0 0 0 10 * ?
- ：表示区间。例如 在小时上设置 "10-12",表示 10,11,12点都会触发。
, ：表示指定多个值，例如在周字段上设置 "MON,WED,FRI" 表示周一，周三和周五触发
/ ：用于递增触发。如在秒上面设置"5/15" 表示从5秒开始，每增15秒触发(5,20,35,50)。 在月字段上设置'1/3'所示每月1号开始，每隔三天触发一次。
L ：表示最后的意思。在日字段设置上，表示当月的最后一天(依据当前月份，如果是二月还会依据是否是润年[leap]), 在周字段上表示星期六，相当于"7"或"SAT"。如果在"L"前加上数字，则表示该数据的最后一个。
例如在周字段上设置"6L"这样的格式,则表示“本月最后一个星期五"
W ：表示离指定日期的最近那个工作日(周一至周五). 例如在日字段上设置"15W"，表示离每月15号最近的那个工作日触发。如果15号正好是周六，则找最近的周五(14号)触发, 如果15号是周未，则找最近的下周一(16号)触发.如果15号正好在工作日(周一至周五)，则就在该天触发。如果指定格式为 "1W",它则表示每月1号往后最近的工作日触发。如果1号正是周六，则将在3号下周一触发。(注，"W"前只能设置具体的数字,不允许区间"-").
'L'和 'W'可以一组合使用。如果在日字段上设置"LW",则表示在本月的最后一个工作日触发
 
# ：序号(表示每月的第几周星期几)，例如在周字段上设置"6#3"表示在每月的第三个周星期六.注意如果指定"6#5",正好第五周没有星期六，则不会触发该配置(用在母亲节和父亲节再合适不过了)
周字段的设置，若使用英文字母是不区分大小写的 MON 与mon相同.
 
Cron表达式示例：
格式: [秒] [分] [小时] [日] [月] [周] [年]
表达式						说明
"0 0 12 * * ? "				每天12点运行
"0 15 10 * * ?"				每天10:15运行
"0 15 10 * * ? 2011"		2011年的每天10：15运行
"0 * 14 * * ?"				每天14点到15点之间每分钟运行一次，开始于14:00，结束于14:59。
"0 0/5 14 * * ?"			每天14点到15点每5分钟运行一次，开始于14:00，结束于14:55。
"0 0/5 14,18 * * ?"			每天14点到15点每5分钟运行一次，此外每天18点到19点每5钟也运行一次。
"0 0-5 14 * * ?"			每天14:00点到14:05，每分钟运行一次。
"0 10,44 14 ? 3 WED"		3月每周三的14:10分到14:44，每分钟运行一次。
"0 15 10 ? * MON-FRI"		每周一，二，三，四，五的10:15分运行。
"0 15 10 15 * ?"			每月15日10:15分运行。
"0 15 10 L * ?"				每月最后一天10:15分运行。
"0 15 10 ? * 6L"			每月最后一个星期五10:15分运行。
"0 15 10 ? * 6L 2007-2009"	在2007,2008,2009年每个月的最后一个星期五的10:15分运行。
"0 15 10 ? * 6#3"			每月第三个星期五的10:15分运行。
0 0 12 1/5 * ?              每月的第一个中午开始每隔5天触发一次 
0 11 11 11 11 ?             每年的11月11号 11点11分触发(光棍节)




maven操作：

部署到Tomcat7:

在project视图中选中项目，右键"Run As"->"Maven bulid"，执行tomcat7:deploy命令，即可完成部署，执行tomcat7:redeploy即可重新部署项目，tomcat7:undeploy卸载部署。

项目主要配置项：
redis配置：redis.properties 注意：redis数据库一定要配置密码（这里指的是redis数据库的密码，然后redis配置文件也要相应配置redis数据库的密码），系统配置的默认密码是：webside
quartz配置：quartz.properties
ehcache配置：ehcache.xml
IP转换配置：ip2region.properties
日志配置：logback.xml
数据库配置：jdbc.properties
项目数据库使用mysql，数据库配置文件位于src/main/resources包下的jdbc.properties，项目连接数据库采用的druid连接池，密码是加密后的，需修改jdbc.url，jdbc.username，jdbc.password，druid.connectionProperties中的config.decrypt.key四个配置项的值为自己数据库对应的值，config.decrypt.key的值就是publicKey，配置方式如下：
D:>java -cp druid-1.0.27.jar com.alibaba.druid.filter.config.ConfigTools 123456(数据库密码)


redis启动命令
cd c:\redis
redis-server.exe redis.windows.conf

初始化Redis密码：
在配置文件中有个参数： requirepass  这个就是配置redis访问密码的参数；
比如 requirepass test123；

关于SSL配置

页面历史
首先要配置tomcat使其支持SSl，具体配置方式如下（我的是tomcat8）：
生成证书：
C:\Users\gaogang>keytool -genkey -alias tomcat -keyalg RSA -keystore D:\keystore.keystore
输入密钥库口令:
再次输入新口令:
您的名字与姓氏是什么?
  [Unknown]:  xxx
您的组织单位名称是什么?
  [Unknown]:  xxx
您的组织名称是什么?
  [Unknown]:  xxx
您所在的城市或区域名称是什么?
  [Unknown]:  xxx
您所在的省/市/自治区名称是什么?
  [Unknown]:  xxx
该单位的双字母国家/地区代码是什么?
  [Unknown]:  CN
CN=xxx, OU=xxx, O=xxx, L=xxx, ST=xxx, C=CN是否正确?
  [否]:  y

输入 <tomcat> 的密钥口令
        (如果和密钥库口令相同, 按回车):

C:\Users\gaogang>
将生成的keystore.keystore复制到tomcat根目录的bin目录下
配置tomcat，修改server.xml：
将这段配置注释去掉：
    <!--
    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
               maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
               clientAuth="false" sslProtocol="TLS" />
    -->
改成如下形式：
<Connector SSLEnabled="true" clientAuth="false" keystoreFile="D:\Program Files\apache-tomcat-8.0.36\bin\keystore.keystore" keystorePass="xxx" maxThreads="150" port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol" scheme="https" secure="true" sslProtocol="TLS"/>
项目使用SSl，即https，目前支持通过两种方式进行配置，一种是通过shiro，添加过滤器实现，一种是通过Java Web的安全验证机制实现，注意两种方式二选一。
第一种： 配置shiro过虑器： 在spring-shiro.xml文件中如下配置：
<bean id="sslFilter" class="org.apache.shiro.web.filter.authz.SslFilter">
        <property name="port" value="8443"/>
</bean>

<!-- 自定义过滤器 -->
<bean id="shiroFilter" class="org.apache.shiro.spring.web.ShiroFilterFactoryBean">
        <!-- shiro的核心安全接口 -->
        <property name="securityManager" ref="securityManager" />
        <!-- 要求登录时的链接 -->
        <property name="loginUrl" value="/login.html" />
        <!-- 登陆成功后要跳转的连接 -->
        <property name="successUrl" value="/index.html" />
        <!-- 未授权时要跳转的连接 -->
        <property name="unauthorizedUrl" value="/denied.jsp" />

        <!-- Shiro连接约束配置,即过滤链的定义 -->
        <!-- 此处可配合我的这篇文章来理解各个过滤连的作用http://blog.csdn.net/jadyer/article/details/12172839 -->
        <!-- 下面value值的第一个'/'代表的路径是相对于HttpServletRequest.getContextPath()的值来的 -->
        <!-- anon：它对应的过滤器里面是空的,什么都没做,这里.do和.jsp后面的*表示参数,比方说login.jsp?main这种 -->
        <!-- authc：该过滤器下的页面必须验证后才能访问,它是Shiro内置的一个拦截器org.apache.shiro.web.filter.authc.FormAuthenticationFilter -->
        <!-- 动态加载权限 -->
        <property name="filterChainDefinitions" value="#{chainDefinitionService.initFilterChainDefinitions()}" />
        <!-- 这里暂时静态加载方式 -->
        <!-- <property name="filterChainDefinitions"> <value> /login.html = authc
            /logout.html = logout /register.html = anon /captcha.html = anon /denied.jsp
            = anon /resources/** = anon /view/error/** = anon /*/withoutAuth/** = anon
            /** = authc </value> </property> -->
        <!-- 自定义过滤器 -->
        <property name="filters">
            <map>
                <entry key="kickout" value-ref="kickoutFilter" />
                <entry key="kickoutAuth" value-ref="kickoutAuthFilter"/>
                <entry key="login" value-ref="loginFilter" />
                <entry key="remember" value-ref="rememberMeFilter"/>
                <entry key="perm" value-ref="permissionFilter"/>
                <entry key="roles" value-ref="roleFilter"/>
                <entry key="baseUrl" value-ref="baseUrl"/>
                 <entry key="ssl" value-ref="sslFilter"></entry>
            </map>
        </property>
    </bean>
在shiroAuth.props文件中做如下配置：

#需要登录,并使用ssl
/**=ssl,login,remember,perm,baseUrl
第二种： web.xml中配置如下节点：
    <security-constraint>
        <web-resource-collection>
            <web-resource-name>webside</web-resource-name>
            <url-pattern>/*</url-pattern>
        </web-resource-collection>
        <user-data-constraint>
            <transport-guarantee>CONFIDENTIAL</transport-guarantee>
        </user-data-constraint>
    </security-constraint>