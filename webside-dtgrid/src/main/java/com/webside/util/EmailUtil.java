package com.webside.util;

import javax.mail.AuthenticationFailedException;

import jodd.mail.Email;
import jodd.mail.MailException;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * @ClassName: EmailUtil
 * @Description: 邮件发送工具类,封装了jodd的mail工具类
 * @author gaogang
 * @date 2016年7月12日 下午4:22:12
 *
 */
@Component
public class EmailUtil {


	@Value("${mail.username}")
	private String MAIL_USER;

	@Value("${mail.password}")
	private String MAIL_PASSWORD;

	@Value("${mail.smtp.server}")
	private String SMTP_SERVER;

	/**
	 * 发送邮箱
	 *
	 * @param toMail
	 * @param subject
	 * @param text
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public boolean sendMail(String toMail, String subject, String text) throws AuthenticationFailedException,MailException{
		Email email = Email.create().from(MAIL_USER).to(toMail)
				.subject(subject).addText(text);
		SmtpServer smtpServer = SmtpServer.create(SMTP_SERVER)
				.authenticateWith(MAIL_USER, MAIL_PASSWORD);
		SendMailSession session = smtpServer.createSession();
		session.open();
		session.sendMail(email);
		session.close();
		return true;
	}
}
