<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/include/form.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath }" />

<style type="text/css">
.noform{
	padding:7px 0px 0px 12px;
	font-size: 14px;
}
.page-header{margin: 10px;}
</style>
<div class="page-header">
	<h3>
		session详情
	</h3>
</div>

<div class="row" style="margin-top:5px;">
	<div class="col-xs-12">
		<form class="form-horizontal" role="form" method="post">
			   <div class="form-group">
			      <label class="control-label col-sm-1 no-padding-right">session ID:</label>
			      <div class="col-sm-10 noform">
			      	${userSessionEntity.sessionId }
			      </div>
			   </div>
			   <div class="form-group">
			      <label class="control-label col-sm-1 no-padding-right">账户:</label>
			      <div class="col-sm-10 noform">
			         ${userSessionEntity.accountName }
			      </div>
			   </div>
			   <div class="form-group">
			      <label class="control-label col-sm-1 no-padding-right">IP:</label>
			      <div class="col-sm-10 noform">
			      	${userSessionEntity.host }
			      </div>
			   </div> 
			   <div class="form-group">
			      <label class="control-label col-sm-1 no-padding-right">创建时间:</label>
			      <div class="col-sm-10 noform">
			         <fmt:formatDate value="${userSessionEntity.startTime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			      </div>
			   </div> 
			   <div class="form-group">
			      <label class="control-label col-sm-1 no-padding-right">最后交互时间:</label>
			      <div class="col-sm-10 noform">
			         <fmt:formatDate value="${userSessionEntity.lastAccess }" pattern="yyyy-MM-dd HH:mm:ss"/>
			      </div>
			   </div>
		</form>
	</div>
</div>
<div class="center">
	<button class="btn btn-info btn-sm" type="button" onclick="$.layer_close()"><i class="fa fa-undo btn_cus"></i>取 消</button>
</div>